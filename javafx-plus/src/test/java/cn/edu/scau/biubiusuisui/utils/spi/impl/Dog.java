package cn.edu.scau.biubiusuisui.utils.spi.impl;

import cn.edu.scau.biubiusuisui.utils.spi.Animal;

/**
 * @author Jade Yeung
 * @time 2022/4/30 11:53
 * @since 1.3.0
 */
public class Dog implements Animal {
    @Override
    public String say() {
        return Dog.class.getSimpleName();
    }
}

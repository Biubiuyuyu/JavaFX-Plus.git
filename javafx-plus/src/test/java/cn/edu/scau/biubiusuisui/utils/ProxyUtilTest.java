package cn.edu.scau.biubiusuisui.utils;

import junit.framework.TestCase;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.junit.Assert;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyUtilTest extends TestCase {

    private Object getJdkDynamicProxy(SimpleController target) {
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                return method.invoke(target, args);
            }
        });
    }

    private SimpleController getCglibProxy(SimpleController target) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                return proxy.invokeSuper(obj, args);
            }
        });
        return (SimpleController) enhancer.create();
    }

    public void testIsJdkDynamicProxy() {
        Assert.assertTrue(ProxyUtil.isJdkDynamicProxy(getJdkDynamicProxy(new SimpleController())));
    }

    public void testIsCglibProxy() {
        Assert.assertTrue(ProxyUtil.isCglibProxy(getCglibProxy(new SimpleController())));
    }

    public void testGetTargetClass() {
        SimpleController proxy = getCglibProxy(new SimpleController());
        Assert.assertEquals(SimpleController.class, ProxyUtil.getTargetClass(proxy));
    }

    public class SimpleControllerInvocationHandler implements InvocationHandler {

        private SimpleController target;

        public SimpleControllerInvocationHandler(SimpleController target) {
            this.target = target;
        }

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            return method.invoke(target, args);
        }

        public Object getProxy() {
            return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
        }
    }
}
package cn.edu.scau.biubiusuisui.utils;

import cn.edu.scau.biubiusuisui.utils.spi.Animal;
import cn.edu.scau.biubiusuisui.utils.spi.Fly;
import cn.edu.scau.biubiusuisui.utils.spi.SayHello;
import cn.edu.scau.biubiusuisui.utils.spi.impl.Dog;
import junit.framework.TestCase;
import org.junit.Assert;

import java.util.List;

public class SpiLoaderUtilTest extends TestCase {

    public void testGetService() {
        Animal animal = SpiLoaderUtil.getService(Animal.class);
        assertEquals(animal.say(), Dog.class.getSimpleName());

        Fly fly = SpiLoaderUtil.getService(Fly.class);
        assertNull(fly);
    }

    public void testTestGetService() {
        Animal animal = SpiLoaderUtil.getService(Animal.class, () -> "Cat");
        assertEquals(animal.say(), Dog.class.getSimpleName());

        Fly fly = SpiLoaderUtil.getService(Fly.class, () -> "飞");
        assertEquals("飞", fly.fly());
    }

    public void testTestGetService1() {
        Animal animal = SpiLoaderUtil.getService(Animal.class, ClassLoader.getSystemClassLoader(), () -> "Cat");
        assertEquals(animal.say(), Dog.class.getSimpleName());

        Fly fly = SpiLoaderUtil.getService(Fly.class, ClassLoader.getSystemClassLoader(), () -> "飞");
        assertEquals("飞", fly.fly());
    }

    public void testTestGetService2() {
        // 测试报错
        try {
            SayHello saySomething = SpiLoaderUtil.getService(SayHello.class, ClassLoader.getSystemClassLoader(), () -> "nihao");
        } catch (IllegalStateException e) {
            Assert.assertEquals("more than one SPI implement class", e.getMessage());
        }
    }

    public void testGetServices() {
        List<SayHello> saySomethings = SpiLoaderUtil.getServices(SayHello.class);
        Assert.assertEquals(2, saySomethings.size());
    }

    public void testTestGetServices() {
        List<SayHello> sayHellos = SpiLoaderUtil.getServices(SayHello.class, ClassLoader.getSystemClassLoader());
        Assert.assertEquals(2, sayHellos.size());
    }
}
package cn.edu.scau.biubiusuisui.function;

import cn.edu.scau.biubiusuisui.annotation.FXWindow;
import cn.edu.scau.biubiusuisui.entity.FXBaseController;
import cn.edu.scau.biubiusuisui.log.FXPlusLoggerFactory;
import cn.edu.scau.biubiusuisui.log.IFXPlusLogger;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author jack
 * @version 1.0
 * @date 2019/6/30 10:40
 * @description 解析@FXWindow
 * @since JavaFX2.0 JDK1.8
 */
public class FXWindowParser {
    private static final IFXPlusLogger logger = FXPlusLoggerFactory.getLogger(FXWindowParser.class);

    public static void parse(FXBaseController fxBaseController, FXWindow fxWindow) {
        logger.info("parsing @FXWindow of class: " + fxBaseController.getControllerName());
        // 初始化Stage
        Stage stage = new Stage();
        double preWidth = fxWindow.preWidth() == 0.0 ? fxBaseController.getPrefWidth() : fxWindow.preWidth();
        double preHeight = fxWindow.preHeight() == 0.0 ? fxBaseController.getPrefHeight() : fxWindow.preHeight();
        stage.setScene(new Scene(fxBaseController, preWidth, preHeight));
        // 设置Stage
        fxBaseController.setStage(stage);

        // 设置 mainStage
        fxBaseController.setMainStage(fxWindow.mainStage());

        // 处理 title
        fxBaseController.setWindowTitle(fxWindow.title());

        // 处理 icon
        fxBaseController.setIcon(fxWindow.icon());

        // 处理draggable和resizable
        if (fxWindow.draggable() || fxWindow.resizable()) {
            fxBaseController.setDragAndResize(fxWindow.draggable(), fxWindow.resizable());
        }

        // fxWindow的resizable默认为false
        if (fxWindow.resizable()) {
            fxBaseController.setDragAndResize(fxWindow.draggable(), true);
        }

        // 处理style
        stage.initStyle(fxWindow.style());

    }
}

package cn.edu.scau.biubiusuisui.proxy;

import cn.edu.scau.biubiusuisui.annotation.FXRedirect;
import cn.edu.scau.biubiusuisui.annotation.FXSender;
import cn.edu.scau.biubiusuisui.entity.FXBaseController;
import cn.edu.scau.biubiusuisui.root.IFXProxy;
import cn.edu.scau.biubiusuisui.signal.SignalQueue;
import cn.edu.scau.biubiusuisui.stage.StageManager;
import cn.edu.scau.biubiusuisui.utils.StringUtil;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

/**
 * This proxy class intercept Methods that has special annotation such as
 * FXSender which is a mark for message queue
 *
 * @author jack
 * @version 1.0
 * @date 2019/6/25 2:03
 * @since JavaFX2.0 JDK1.8
 */
public class FXControllerProxy implements MethodInterceptor, IFXProxy<FXBaseController> {

    private FXBaseController target;

    public FXControllerProxy(FXBaseController target) {
        this.target = target;
    }

    @Override
    public FXBaseController getEnhancer() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(this.target.getClass());
        enhancer.setCallback(this);
        Object proxy = enhancer.create();
        // target.* -> proxy.*
        inject(target, proxy);

        FXBaseController fxBaseController = (FXBaseController) proxy;
        fxBaseController.loadFXBaseController(); //加载相关属性
        return fxBaseController;
    }

    @Override
    public FXBaseController getTarget() {
        return this.target;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        //获取该方法运行后的结果
        Object invokeResult = methodProxy.invokeSuper(o, objects);

        for (Annotation annotation : method.getDeclaredAnnotations()) {
            // 处理发送信号的函数
            handleFXSender(annotation, method, invokeResult);
            // 处理重定向的函数
            handleFXRedirect(annotation, invokeResult);
        }
        return invokeResult;
    }

    /**
     * 处理FXSender注解
     *
     * @param annotation   注解
     * @param method       方法
     * @param invokeResult 方法执行结果
     */
    private void handleFXSender(Annotation annotation, Method method, Object invokeResult) {
        if (FXSender.class != annotation.annotationType()) {
            return;
        }
        FXSender fxSender = (FXSender) annotation;
        String prefix = target.getControllerName() + ":";
        List<String> nameList = new LinkedList<>();
        for (String name : fxSender.name()) {
            String tmpName = StringUtil.isBlank(name) ? prefix + method.getName() : prefix + name;
            nameList.add(tmpName);
        }
        SignalQueue.getInstance().sendMsg(nameList, invokeResult);
    }

    /**
     * 处理FXRedirect注解
     *
     * @param annotation   注解
     * @param invokeResult 方法执行结果
     */
    private void handleFXRedirect(Annotation annotation, Object invokeResult) {
        if (FXRedirect.class != annotation.annotationType()) {
            return;
        }
        FXRedirect fxRedirect = (FXRedirect) annotation;
        if (fxRedirect.close()) {  //关闭原窗口
            StageManager.getInstance().closeStage(target.getControllerName());
        }
        StageManager.getInstance().redirectTo(invokeResult);
    }

    private void inject(Object target, Object proxy) {
        Class<?> clazz = target.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                Object value = field.get(target);
                field.set(proxy, value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}

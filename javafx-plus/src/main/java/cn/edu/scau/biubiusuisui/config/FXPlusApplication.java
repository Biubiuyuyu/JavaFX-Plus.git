package cn.edu.scau.biubiusuisui.config;

import cn.edu.scau.biubiusuisui.annotation.FXScan;
import cn.edu.scau.biubiusuisui.annotation.FXWindow;
import cn.edu.scau.biubiusuisui.exception.NotFXControllerException;
import cn.edu.scau.biubiusuisui.factory.BeanBuilder;
import cn.edu.scau.biubiusuisui.factory.FXBuilder;
import cn.edu.scau.biubiusuisui.factory.FXControllerFactory;
import cn.edu.scau.biubiusuisui.function.FXWindowParser;
import cn.edu.scau.biubiusuisui.log.FXPlusLoggerFactory;
import cn.edu.scau.biubiusuisui.log.IFXPlusLogger;
import cn.edu.scau.biubiusuisui.utils.ClassUtil;
import cn.edu.scau.biubiusuisui.utils.FileUtil;
import cn.edu.scau.biubiusuisui.utils.LogUtil;
import javafx.application.Platform;

import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author jack
 * @version 1.0
 * @date 2019/6/25 2:54
 * @since JavaFX2.0 JDK1.8
 */
public class FXPlusApplication {
    private static final IFXPlusLogger logger = FXPlusLoggerFactory.getLogger(FXPlusApplication.class);

    private static FXWindowParser windowAnnotationParser = new FXWindowParser();

    private static BeanBuilder DEFAULT_BEAN_FACTORY = new FXBuilder();

    private static BeanBuilder beanBuilder;

    public static boolean IS_SCENE_BUILDER = true;

    public static void start(Class<?> clazz, BeanBuilder beanBuilder) {
        logger.info("starting JavaFX-Plus Application");
        try {
            logger.info("\n" + FileUtil.readFileFromResources("banner.txt"));
        } catch (UnsupportedEncodingException e) {
            logger.error("\n read classpath:banner.txt error, you can ignore it");
        }
        // 初始化日志路径
        LogUtil.initLog4jBase();

        IS_SCENE_BUILDER = false;
        FXPlusApplication.beanBuilder = beanBuilder;
        Annotation[] annotations = clazz.getDeclaredAnnotations();
        logger.info("starting to scanning and registering controllers");
        for (Annotation annotation : annotations) {
            // 加载扫描类
            handleFXScan(annotation);
        }
    }

    /**
     * 处理FXScan注解
     *
     * @param annotation 注解
     */
    private static void handleFXScan(Annotation annotation) {
        if (FXScan.class != annotation.annotationType()) {
            return;
        }
        String[] dirs = ((FXScan) annotation).base();
        Set<String> sets = new HashSet<>();
        Collections.addAll(sets, dirs);
        //Set<String> classNames = new HashSet<>();
        for (String dir : sets) {
            logger.info("scanning directory: " + dir);
            ClassUtil classUtil = new ClassUtil();
            List<String> temps;
            try {
                temps = classUtil.scanAllClassName(dir);
                for (String className : temps) {
                    logger.info("loading class: " + className);
                    loadFXPlusClass(className, beanBuilder);
                }
            } catch (UnsupportedEncodingException | ClassNotFoundException | NotFXControllerException exception) {
                logger.error("{}", exception);
            }
        }
    }

    public static void start(Class<?> clazz) {
        start(clazz, true);
    }

    /**
     * @param implicitExit 关闭最后一个Stage时，是否退出JavaFX应用程序，true-是，false-否
     * @since 1.3 添加implicitExit属性
     */
    public static void start(Class<?> clazz, boolean implicitExit) {
        Platform.setImplicitExit(implicitExit);
        start(clazz, DEFAULT_BEAN_FACTORY);
    }

    private static void loadFXPlusClass(String className, BeanBuilder beanBuilder) throws ClassNotFoundException, NotFXControllerException {
        Class<?> clazz = Class.forName(className);
        // 是窗口，需要初始化Stage
        if (Objects.nonNull(clazz.getAnnotation(FXWindow.class))) {
            logger.info("loading stage of class: " + className);
            FXControllerFactory.loadStage(clazz, beanBuilder);
        }
    }
}

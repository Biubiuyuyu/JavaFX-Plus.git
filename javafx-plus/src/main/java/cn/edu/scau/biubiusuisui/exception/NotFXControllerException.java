package cn.edu.scau.biubiusuisui.exception;

/**
 * 某Controller缺少FXController注解一场
 *
 * @author Jade Yeung
 * @time 2022/5/2 22:37
 * @since 1.3.0
 */
public class NotFXControllerException extends Exception {

    public NotFXControllerException(String className) {
        super("Can not find annotation @FXController in class: " + className);
    }
}

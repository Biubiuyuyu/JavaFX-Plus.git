package cn.edu.scau.biubiusuisui.example.windowDemo;

import cn.edu.scau.biubiusuisui.annotation.FXController;
import cn.edu.scau.biubiusuisui.annotation.FXWindow;
import cn.edu.scau.biubiusuisui.entity.FXBaseController;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleButton;
import javafx.stage.StageStyle;

/**
 * @author suisui
 * @description 测试Controller
 * @date 2020/8/29 09:41
 * @since JDK1.8
 */
@FXWindow(mainStage = true, title = "windowDemo", icon = "image/icon.png", style = StageStyle.UNDECORATED)
@FXController(path = "fxml/windowDemo/windowDemo.fxml")
public class DemoController extends FXBaseController {
    private String title = "windowDemo -- ";
    private int count = 0;
    private String iconStr = "image/icon2.png";
    private String iconStr2 = "image/icon3.png";


    @FXML
    private ToggleButton canResizableTB;

    @Override
    public void initialize() throws Exception {
        canResizableTB.selectedProperty().addListener(e -> {
            this.setDragAndResize(true, canResizableTB.isSelected());
        });
    }

    /**
     * 修改标题点击事件
     */
    @FXML
    public void changeTitleClick() {
        this.setWindowTitle(title + count);
        count++;
    }

    /**
     * 字符串修改图标
     */
    @FXML
    public void changeIconClick() {
        this.setIcon(count % 2 == 0 ? iconStr : iconStr2);
        count++;
    }

}

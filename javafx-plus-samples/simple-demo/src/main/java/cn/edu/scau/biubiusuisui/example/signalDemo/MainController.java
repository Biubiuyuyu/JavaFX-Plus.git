package cn.edu.scau.biubiusuisui.example.signalDemo;

import cn.edu.scau.biubiusuisui.annotation.FXController;
import cn.edu.scau.biubiusuisui.annotation.FXReceiver;
import cn.edu.scau.biubiusuisui.annotation.FXWindow;
import cn.edu.scau.biubiusuisui.entity.FXBaseController;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

/**
 * @author suisui
 * @version 1.2
 * @description 主界面
 * @date 2019/12/8 13:17
 * @since JavaFX2.0 JDK1.8
 */
@FXController(path = "fxml/signalDemo/main.fxml")
@FXWindow(mainStage = true, title = "SignalDemo")
public class MainController extends FXBaseController {

    @FXML
    private TextArea outTA;

    @FXML
    private TextArea outTB;

    /**
     * 接收者必须指定要订阅的[发送者类名:方法名]
     * 发送函数的返回值会注入到接收函数的参数中
     *
     * @param msg
     */
    @FXReceiver(name = {"TopBarController:sendToMain"})
    public void handleTopBarA(String msg) {
        // 处理导航栏的点击事件
        outTA.appendText(msg + "-A0" + "\n");
    }

    @FXReceiver(name = {"TopBarController:sendToMain"})
    public void handleTopBarA1(String msg) {
        // 处理导航栏的点击事件
        outTA.appendText(msg + "-A1" + "\n");
    }

    @FXReceiver(name = {"TopBarController:sendToMain"})
    public void handleTopBarA2(String msg) {
        // 处理导航栏的点击事件
        outTA.appendText(msg + "-A2" + "\n");
    }

    @FXReceiver(name = {"TopBarController:sendToMain"})
    public void handleTopBarA3(String msg) {
        // 处理导航栏的点击事件
        outTA.appendText(msg + "-A3" + "\n");
    }

    @FXReceiver(name = {"TopBarController:sendToMain"})
    public void handleTopBarA4(String msg) {
        // 处理导航栏的点击事件
        outTA.appendText(msg + "-A4" + "\n");
    }

    @FXReceiver(name = {"TopBarController:sendToMainB"})
    public void handleTopBarB(String msg) {
        // 处理导航栏的点击事件
        outTB.appendText(msg + "-B0" + "\n");
    }

    @FXReceiver(name = {"TopBarController:sendToMainB"})
    public void handleTopBarB1(String msg) {
        // 处理导航栏的点击事件
        outTB.appendText(msg + "-B1" + "\n");
    }

    @FXReceiver(name = {"TopBarController:sendToMainB"})
    public void handleTopBarB2(String msg) {
        // 处理导航栏的点击事件
        outTB.appendText(msg + "-B2" + "\n");
    }


    @FXReceiver(name = {"TopBarController:sendToMainB"})
    public void handleTopBarB3(String msg) {
        // 处理导航栏的点击事件
        outTB.appendText(msg + "-B3" + "\n");
    }

    @FXReceiver(name = {"TopBarController:sendToMainB"})
    public void handleTopBarB4(String msg) {
        // 处理导航栏的点击事件
        outTB.appendText(msg + "-B4" + "\n");
    }
}
